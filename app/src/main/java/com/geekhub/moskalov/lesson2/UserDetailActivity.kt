package com.geekhub.moskalov.lesson2

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.geekhub.moskalov.lesson2.model.Contact
import com.geekhub.moskalov.lesson2.tools.BitmapTool
import kotlinx.android.synthetic.main.fragment_main.*
import java.io.IOException


class UserDetailActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var user: Contact
    private val GALLERY_REQUEST = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        button.setOnClickListener(this)

        user = intent.getSerializableExtra("contact") as Contact

        name.text = String.format("Name: %s", user.name)
        surname.text = String.format("Surname: %s", user.surname)
        age.text = String.format("Age: %s", user.age.toString())
        phone.text = String.format("Phone: %s", user.phone)
        if (user.image.isNotEmpty()) {
            imageView.setImageBitmap(BitmapTool.getBitMap(this.contentResolver, Uri.parse(user.image)))
        } else {
            imageView.setImageResource(0)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button -> {
                val photoPickerIntent = Intent(Intent.ACTION_PICK)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var bitmap: Bitmap? = null
        val imageView = findViewById<View>(R.id.imageView) as ImageView

        when (requestCode) {
            GALLERY_REQUEST -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = data?.data
                try {
                    selectedImage?.let {
                        user.image = it.toString()
                        bitmap = BitmapTool.getBitMap(this.contentResolver, it)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                imageView.setImageBitmap(bitmap)

            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra("contact", user)
        }
        startActivity(intent)
        super.onSaveInstanceState(outState)
    }
}
