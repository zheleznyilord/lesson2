package com.geekhub.moskalov.lesson2.adapters

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_main.view.*
import com.geekhub.moskalov.lesson2.R
import com.geekhub.moskalov.lesson2.UserDetailActivity
import com.geekhub.moskalov.lesson2.model.Contact

class MainRecyclerAdapter(private val callback: AdapterCallback) : RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>(){
    interface AdapterCallback {
        fun onLongItemClick(contact: Contact)
        fun onItemClick(contact: Contact)
    }

    private val items = mutableListOf<Contact>()

    fun getItems(): MutableList<Contact> {
        return items
    }

    fun clearAllItems(){
        items.clear()
    }

    fun addItem(contact: Contact) {
        items.add(contact)
        notifyItemInserted(items.size)
    }


    fun deleteItem(contact: Contact) {
        val position = items.indexOf(contact)
        items.remove(contact)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_main, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = items.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (items[position].selected){
            holder.itemView.setBackgroundColor(Color.argb(211, 0, 255, 255))
        } else {
            holder.itemView.setBackgroundColor(Color.argb(255, 255, 255, 255))
        }
        holder.onBind(contact = items[position])
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view),
        View.OnLongClickListener, View.OnClickListener {


        private lateinit var contact: Contact

        init {
            view.setOnLongClickListener(this)
            view.setOnClickListener(this)
        }

        fun onBind(contact: Contact) {
            view.name_edited.text = contact.name
            view.age_edited.text = contact.age.toString()
            view.surname_edited.text = contact.surname
            view.phone_edited.text = contact.phone
            this.contact = contact
        }

        override fun onLongClick(v: View?): Boolean {
            callback.onLongItemClick(contact)
            return true
        }

        override fun onClick(v: View?) {
            callback.onItemClick(contact)
            var i = items.indexOf(contact)
            //notifyItemChanged(i)
            notifyDataSetChanged()
        }

    }
}