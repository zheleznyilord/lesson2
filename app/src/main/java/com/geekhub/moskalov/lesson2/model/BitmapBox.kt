package com.geekhub.moskalov.lesson2.model

import android.graphics.Bitmap
import java.io.Serializable

class BitmapBox(bitmap: Bitmap) : Serializable{
    private var pixels: IntArray = IntArray(0)
    private var width: Int = 0
    var height: Int = 0

    init {
        width = bitmap.width
        height = bitmap.height
        pixels = IntArray(width*height)
        bitmap.getPixels(pixels,0,width,0,0,width,height)
    }

    fun setBitmap(bitmap: Bitmap){
        width = bitmap.width
        height = bitmap.height
        pixels = IntArray(width*height)
        bitmap.getPixels(pixels,0,width,0,0,width,height)
    }

    fun getBitmap(): Bitmap {
        return Bitmap.createBitmap(pixels, width, height, Bitmap.Config.ARGB_8888)
    }
}