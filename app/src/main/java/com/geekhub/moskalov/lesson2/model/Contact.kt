package com.geekhub.moskalov.lesson2.model

import android.graphics.Bitmap
import android.net.Uri
import java.io.Serializable
import java.net.URI

data class Contact (val name: String,
                    val surname: String,
                    val phone: String,
                    val age: Int,
                    var image: String,
                    var selected: Boolean) : Serializable