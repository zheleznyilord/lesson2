package com.geekhub.moskalov.lesson2.tools

import android.content.ContentResolver
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore

class BitmapTool {


    companion object {
        fun getBitMap(cr: ContentResolver, uri: Uri): Bitmap {
            return if (Build.VERSION.SDK_INT < 28) {
                MediaStore.Images.Media.getBitmap(cr, uri)
            } else {
                val source = ImageDecoder.createSource(cr, uri)
                ImageDecoder.decodeBitmap(source)
            }
        }
    }


}