package com.geekhub.moskalov.lesson2

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import com.geekhub.moskalov.lesson2.adapters.MainRecyclerAdapter
import com.geekhub.moskalov.lesson2.model.Contact
import com.geekhub.moskalov.lesson2.tools.BitmapTool
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.*
import java.io.IOException


class MainActivity : AppCompatActivity(), MainRecyclerAdapter.AdapterCallback,
    View.OnClickListener {

    private var GALLERY_REQUEST = 0
    private val adapter by lazy { MainRecyclerAdapter(this) }
    private val fragment by lazy { MainFragment() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view.adapter = adapter



        adapter.addItem(Contact("Vadim", "Moskalov", "8805353535", 24, Uri.EMPTY.toString(),
            false))
        adapter.addItem(Contact("Anton", "Stepanov", "102", 25, Uri.EMPTY.toString(),
            false))
        adapter.addItem(Contact("Vlad", "Antonov", "911", 26, Uri.EMPTY.toString(),
            false))
        adapter.addItem(Contact("Stepan", "Averin", "112", 27, Uri.EMPTY.toString(),
            false))

        add_contact.setOnClickListener(this)
        if (resources.configuration.orientation.equals(Configuration.ORIENTATION_LANDSCAPE)) {
            button.setOnClickListener(this)
            val data = intent.getSerializableExtra("contact")
            if (data != null) {
                val user = data as Contact

                name.text = String.format("Name: %s", user.name)
                surname.text = String.format("Surname: %s", user.surname)
                age.text = String.format("Age: %s", user.age.toString())
                phone.text = String.format("Phone: %s", user.phone)
            }
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        val bundle = Bundle()
        bundle.putInt("count", adapter.itemCount)
        val items = adapter.getItems()
        for (item in items) {
            bundle.putSerializable(items.indexOf(item).toString(), item)
        }
        outState.putBundle("contacts", bundle)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        val bundle = savedInstanceState.getBundle("contacts")
        val count = bundle!!.getInt("count") - 1
        adapter.clearAllItems()
        for (i in 0..count) {
            val item: Contact = bundle.getSerializable(i.toString()) as Contact
            adapter.addItem(item)
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.add_contact -> {
                val name = name_edit.text.toString()
                val surname = surname_edit.text.toString()
                val phone = phone_edit.text.toString()
                val age = age_edit.text.toString().toInt()
                val contact = Contact(
                    name,
                    surname,
                    phone,
                    age,
                    Uri.EMPTY.toString(),
                    false
                )
                adapter.addItem(contact)
                val builder = NotificationCompat.Builder(this)
                    .setSmallIcon(android.R.mipmap.sym_def_app_icon)
                    .setContentTitle("Moskalov application")
                    .setContentText("Contact be added")

                val notification = builder.build()

                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(1, notification)

            }
            R.id.button -> {
                val photoPickerIntent = Intent(Intent.ACTION_PICK)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resources.configuration.orientation.equals(Configuration.ORIENTATION_LANDSCAPE)) {
            var bitmap: Bitmap? = null
            val imageView = findViewById<View>(R.id.imageView) as ImageView

            when (requestCode) {
                GALLERY_REQUEST -> if (resultCode == Activity.RESULT_OK) {
                    val selectedImage = data?.data

                    try {
                        selectedImage?.let {
                            adapter.getItems()[GALLERY_REQUEST].image = it.toString()
                            bitmap = BitmapTool.getBitMap(this.contentResolver, it)
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    imageView.setImageBitmap(bitmap)

                }
            }
        }
    }

    override fun onLongItemClick(contact: Contact) {
        adapter.deleteItem(contact)
    }

    override fun onItemClick(contact: Contact) {
        for (item in adapter.getItems()) {
            item.selected = false
        }
        contact.selected = true
        if (resources.configuration.orientation.equals(Configuration.ORIENTATION_PORTRAIT)) {
            val intent = Intent(this, UserDetailActivity::class.java).apply {
                putExtra("contact", contact)
            }
            startActivity(intent)
        } else {
            name.text = String.format("Name: %s", contact.name)
            surname.text = String.format("Surname: %s", contact.surname)
            age.text = String.format("Age: %s", contact.age.toString())
            phone.text = String.format("Phone: %s", contact.phone)
            if(contact.image.isNotEmpty()){
                imageView.setImageBitmap(BitmapTool.getBitMap(this.contentResolver, Uri.parse(contact.image)))
            } else{
               imageView.setImageResource(0)
            }
            //recycler_view[0].setBackgroundColor(Color.argb(255, 255, 255, 255))
        }
    }
}